<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
<%@ include file="../common/comm_css.jsp"%>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>项目页面</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title" style="height: 50px;padding-top: 10px;">
							<h5 style="margin-top: 8px;">项目页面</h5>
							<div class="ibox-tools">
	                           <button class="btn btn-primary" onclick="location.href='add_pro.jsp'">新增项目</button>
                        	</div>
						</div>
						<div class="ibox-content">
							<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>SN</th>
											<th>项目名称</th>
											<th>项目描述</th>
											<th>创建时间</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody id="listgroup">
										<!-- 
											<tr>
												<td>#{list[i].id}</td>
												<td>#{list[i].project_name}</td>
												<td>#{list[i].project_remark}</td>
												<td>@{getTime}</td>
												<td>@{getOpt}</td>
											</tr>
										 -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>

</html>
<script type="text/javascript">
	XCOTemplate.pretreatment('listgroup');	
	
	getProList();
	function getProList() {
		var xco = new XCO();
		//xco.setIntegerValue("start", _start);
		//xco.setIntegerValue("pageSize", _pageSize);

		var options = {
			url : "/manager/getProList.xco",
			data : xco,
			success : getProListCallBack
		};
		$.doXcoRequest(options);
	}

	function getProListCallBack(data) {
		if (data.getCode() != 0) {
			layer.msg(data.getMessage());
		} else {
			var total = 0;
			total = data.getIntegerValue("total");
			num = total;
			var len = 0;
			var list = data.getXCOListValue("list");
			
			var html = "";
			if (list) {
				len = list.length;
			}
			var extendedFunction = {
				getTime : function(){
					return list[i].getStringValue("create_time");
				},
				getOpt : function(){
					return '<a href="interface.jsp?pro_id='+list[i].getLongValue("id")+'&&module_id='+list[i].getLongValue("module_id")+'">查看</a>&nbsp;&nbsp;&nbsp;<a href="edit_pro.jsp?pro_id='+list[i].getLongValue("id")+'">编辑</a>';
				}
			};
			for ( var i = 0; i < len; i++) {
				data.setIntegerValue("i", i);
				html += XCOTemplate.execute('listgroup', data, extendedFunction);
			}
			document.getElementById('listgroup').innerHTML = html;
		}
	}
</script>