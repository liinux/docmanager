package com.gatherlife.manager.servlet;

import java.io.Writer;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.xson.common.object.XCO;
import org.xson.tangyuan.cache.CacheComponent;
import org.xson.tangyuan.cache.TangYuanCache;
import org.xson.tangyuan.web.AbstractPermissionFilter;
import org.xson.tangyuan.web.RequestContext;

import com.gatherlife.manager.Constant;
import com.gatherlife.manager.utils.CookieUtil;

public class PlatformUserFilter extends AbstractPermissionFilter {

	private static Logger log = Logger.getLogger(PlatformUserFilter.class);

	@Override
	public boolean permissionCheck(String permission, RequestContext requestContext) {
		boolean result = false;
		try {
			String url = requestContext.getUrl();
			
			if("/userlogin".equals(url) || "/userexit".equals(url) || "/sendSms".equals(url) || "syslogin".endsWith(url)
					|| "/sysexit".equals(url) || "/phone/getPhotoListU14".equals(url) || "/show/showNoYearMonthList".equals(url)
					|| "/phone/newslistN15".equals(url) || "/phone/newsInfoN16".equals(url) || "/phone/multiInfoN17".equals(url)
					|| "/phone/singleInfoN18".equals(url) || "/register".equals(url) || "/forgetpwd".equals(url) 
					|| "/show/showNoList".equals(url) || "/show/showEditionList".equals(url) || "/show/showJpgList".equals(url) 
					|| "/show/showOneJpg".equals(url) || "/down2".equals(url) || "/getHotList".equals(url) 
					|| "/show/getOneNews".equals(url) || "/show/showLastNo".equals(url) || "/show/getNewsList".equals(url)
					|| "/show/showNewsList".equals(url)
					){
				
				result = true;
				
			}else{
				// 获取权限内容, 从用户session
				//XCO tokenUser = (XCO)requestContext.getRequest().getSession().getAttribute("tokenUser");
				// 获取token
				String token = CookieUtil.getToken(requestContext.getRequest(), Constant.PLATFORM_COOKIE_NAME_TOKEN);
				if (null == token) {
					return false;
				}
				
				token = URLDecoder.decode(token, "utf-8");
				
				TangYuanCache redis = CacheComponent.getInstance().getCache("cache1");
				
				// 获取内容
				String xml = (String)redis.get(Constant.PLATFORM_USER_TOKEN_PREFIX + token);
				if (null == xml) {
					return false;
				}
				
				XCO tokenUser = XCO.fromXML(xml);		
				
				redis.put(Constant.SYSTEM_USER_TOKEN_PREFIX + token, tokenUser.toXMLString(), Constant.REDIS_COOKIE_TOKEN_EXPIRE);
				// 放入内容
				requestContext.getAttach().put("PLATFORM_USER", tokenUser);
				
				if(null != tokenUser){
					long type = tokenUser.getLongValue("role_id");
					if(null!=permission && !"".equals(permission)){
						if(permission.contains(type+"")){
							result=true;
						}
					}else{
						result=true;
					}
				}
			}
		} catch (Throwable e) {
			log.error("权限验证失败:", e);
		}
		return result;
	}

	@Override
	public void authFailed(RequestContext requestContext) {
		try {
			if (requestContext.isAjax()) {
				HttpServletResponse response = requestContext.getResponse();
				response.setContentType("text/xml;charset=utf-8");
				response.setCharacterEncoding("UTF-8");
				Writer write = response.getWriter();
				// TODO
				//write.write(XCOUtil.getResult(100, "无权限访问").toXMLString());
				write.close();
			} else {
				// TODO
				requestContext.getResponse().sendRedirect("index.jsp");
			}
		} catch (Throwable e) {
			log.error(e);
		}
	}
}
